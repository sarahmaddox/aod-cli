rem *** Hallo, I'm the Atlassian AOD CLI solution.
rem *** This is the main [ADD PRODUCT NAME] bat file, containing pages to be copied to the AOD space. 


@echo off
rem remember the directory path to this bat file
set dirPath=%~dp0

rem need to reverse windows names to posix names by changing \ to /
set dirPath=%dirPath:\=/%
rem remove blank at end of string
set dirPath=%dirPath:~0,-1%

rem Get server, username and password

set /p server= "Server: "
set /p username= "Username: "
set /p password= "Password: "

rem Start off the logging to a text file
echo Atlassian AOD CLI report in "PRODUCTDocsLog.txt" > PRODUCTDocsLog.txt
echo START OF REPORT >> PRODUCTDocsLog.txt

@echo on

rem [ADD YOUR COMMANDS BELOW THIS COMMENT.] Example:
rem java -jar "%dirPath%"/lib/confluence-cli-2.6.0.jar --server %server% --user %username% --password %password% --action copyPage --space "DOC" --title "Getting Started" --newSpace "AODCLI" --replace --copyAttachments --noConvert --parent "Confluence User's Guide" >> PRODUCTDocsLog.txt

echo END OF REPORT >> PRODUCTDocsLog.txt
rem *** That's it, I've finished! The log is in "PRODUCTDocsLog.txt"
rem *** Bye!
@echo off

pause

rem Exit with the correct error level.
EXIT /B %ERRORLEVEL%